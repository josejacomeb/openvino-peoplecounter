/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var str_entry = "entry";
var str_exit = "exit";
var str_people = "people";
// Create a client instance
client = new Paho.MQTT.Client(location.hostname, 1883, "clientId");
// set callback handlers
client.onMessageArrived = onMessageArrived;
// connect the client
client.connect({onSuccess: onConnect});

// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    client.subscribe(str_entry);
    client.subscribe(str_exit);
    client.subscribe(str_people);
    console.log("Success!");
}

// called when a message arrives
function onMessageArrived(message) {
    if (message.destinationName.localeCompare(str_entry) === 0) {
        $("#entering").text(message.payloadString);
    } else if (message.destinationName.localeCompare(str_exit) === 0) {
        $("#leaving").text(message.payloadString);
    } else if (message.destinationName.localeCompare(str_people) === 0) {
        $("#passengers").text(message.payloadString);
    }
}

/*client.on("message", function (topic, payload) {
 
 if (topic.localeCompare(str_entry) > 0) {
 $("#entering").text(payload.toString());
 } else if (topic.localeCompare(str_exit) > 0) {
 $("#exiting").text(payload.toString());
 } else if (topic.localeCompare(str_people) > 0) {
 $("#people").text(payload.toString());
 }
 
 });*/
    